package test;

import static org.junit.Assert.*;
import max.Algorithm;

import org.junit.Test;

public class AlgorithTest {

	@Test
	public void test() {
		int[] arraySrc = {1,2,3,4,5,6,7,8,9,10};
		int[] arrayDst = {6,7,8,9,10};

		Algorithm algo = new Algorithm();
		
		assertEquals(5, algo.findPosition(arraySrc, arrayDst));
		
	}

	@Test
	public void test1() {
		int[] arraySrc = {1,2,3,4,5,6,7,8,9,10};
		int[] arrayDst = {6,7,8,10};

		Algorithm algo = new Algorithm();
		
		assertEquals(-1, algo.findPosition(arraySrc, arrayDst));
	}
	
	@Test
	public void test2() {
		int[] arraySrc = {1};
		int[] arrayDst = {1};

		Algorithm algo = new Algorithm();
		
		assertEquals(0, algo.findPosition(arraySrc, arrayDst));
	}
	
	@Test
	public void test3() {
		int[] arraySrc = {1,2,3,4,5,6,7,8,9,10};
		int[] arrayDst = {6,7,8,9,10,11};

		Algorithm algo = new Algorithm();
		
		assertEquals(-1, algo.findPosition(arraySrc, arrayDst));
	}
	
}
