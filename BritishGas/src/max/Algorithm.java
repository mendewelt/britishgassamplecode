package max;

public class Algorithm {
	public static void main(String[] args) {
		
	}
	
	public Algorithm() {
		// TODO Auto-generated constructor stub
	}
 
	/**
	 * Given 2 arrays, find the starting position of the second array in the first array
	 * @param arraySource the first array
	 * @param arrayDest the second array
	 * @return the index of the first array where the second array start, return -1 else*/
	public int findPosition(int[] arraySource, int[] arrayDest){
		
		if(arraySource.length < 1 || arrayDest.length < 1)
			return -1;
		
		for(int index = 0; index < arraySource.length ; index++){
			boolean hasArray = true;
			if(arraySource[index] == arrayDest[0]){	//inner array maybe found
				
				for(int destIndex = 0; destIndex < arrayDest.length; destIndex++){
					//Test if the first array is out of bound 
					if(destIndex + index >= arraySource.length ||  arrayDest[destIndex] != arraySource[destIndex + index] ){
						hasArray = false;
						break; //No need to continue the loop
					}
				}
				
				if(hasArray)
					return index;
			}
		}
		
		return -1;
		
	}
}
